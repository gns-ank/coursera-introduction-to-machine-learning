#!/usr/bin/python

import numpy as np

# Functions
def scale(array, direction=0):
    """
    This scales the matrix (but only the columns 1:, the first contains
    ones and is copied over).
    """
    r = np.array(array)
    if direction == 0:
        for id_ in range(1, array.shape[1]):
            i = r[:,id_]
            r[:,id_] = (i - i.mean())/(np.std(i))
    else:
        raise NotImplementedError("This is not implemented yet")

    return r

def hypothesis(theta, parameter_data):
    """
    Calculates the hypothesis vector for all training samples:

    h_i = theta^T * x^(i) = sum_j theta_j * x_j^(i)

    This means that theta_0 will be multiplied with each column of the
    vector and then each member in the row will be added to form a new
    vector.
    """
    return np.dot(parameter_data, theta)

def cost_function(theta, parameter_data, data_points):
    """
    This calculates the cost function, so that we can monitor its
    decrease whilst the simulation is being run.
    """
    d = hypothesis(theta, parameter_data) - data_points

    return np.dot(d, d) / (2 * parameter_data.shape[0])

def cost_derivative(theta, X, data_points):
    """
    This is the term needed in the gradient descent algorithm.

    This is a vector of dimentionality n + 1 (where n is the number of
    features).

    Here we are calculating a vector:
        b_j = sum_i (h_i - y_i) * x_j^(i)
    """
    return np.dot(
        hypothesis(theta, X) - data_points,
        X)

def calc_step(alpha, theta, X, Y):
    return theta - alpha * cost_derivative(theta, X, Y)

def gradient_descent(iterations, X, Y, alpha):
    theta = np.zeros(X.shape[1])

    for i in range(iterations):
        print("The cost function value is {:.5E}. {} iteration.".format(
            cost_function(theta, X, Y),
            i))
        theta = calc_step(alpha, theta, X, Y)

    return theta


def main():
    # Given
    n = 2
    m = 4

    x = np.ones((m, n + 1))
    x[:,1] = np.array([89, 72, 94, 69])
    x[:,2] = x[:,1]**2

    y = np.array([96, 74, 87, 78])
    alpha = 0.1

    x_scaled = scale(x)

    print("The non-scaled values are:\n{}\n".format(x))
    print("The scaled values are:\n{}\n".format(x_scaled))

    th = gradient_descent(75, x_scaled, y, alpha)
    print("The answer is: {}".format(th))

if __name__ == "__main__":
    main()
